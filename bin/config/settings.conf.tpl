#!/bin/bash

PROJECT_NAME='project'
PROJECT_LEVEL='devt' # base, devt, test, prod

CONFIG_SOURCE=`dirname "$BASH_SOURCE"`
SCRIPT_SOURCE=$CONFIG_SOURCE/..

PROJ_DIR=$SCRIPT_SOURCE/..

ENV_DIR=$PROJ_DIR/../env
REQ_DIR=$PROJ_DIR/requirements
LOG_DIR=$PROJ_DIR/../var/logs
BIN_DIR=$PROJ_DIR/../bin
BACKUP_DIR=$PROJ_DIR/../backup
STATIC_DIR=$PROJ_DIR/../frontend/static
MEDIA_DIR=$PROJ_DIR/../frontend/media
BOWER_DIR=$PROJ_DIR/../frontend/components
