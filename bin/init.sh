#!/bin/bash

source $(cd $(dirname "$0"); pwd)/config/settings.conf

echo "Copying sublime project files outside"
cp $SCRIPT_SOURCE/templates/project.sublime-project.tpl $PROJ_DIR/../$PROJECT_NAME.sublime-project

echo "Copying local_settings template file for configuring"
cd $PROJ_DIR
cp ./conf/local_settings.tpl ./conf/local_settings.py

echo "Re-initializing the git project"
cd $PROJ_DIR
rm -rf .git/
git init
git add --all
git commit
