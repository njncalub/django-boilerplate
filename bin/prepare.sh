#!/bin/bash

source $(cd $(dirname "$0"); pwd)/config/settings.conf

echo "Checking virtual environment..."
if [ -x "$(command -v deactivate)" ]; then deactivate; fi
test -d $ENV_DIR || virtualenv --no-site-packages --prompt="($PROJECT_NAME)" $ENV_DIR
source $ENV_DIR/bin/activate

echo "Installing project requirements for $PROJECT_LEVEL..."
pip install -r $REQ_DIR/$PROJECT_LEVEL.txt

echo "Creating migrations..."
python $PROJ_DIR/manage.py makemigrations

echo "Applying migrations..."
python $PROJ_DIR/manage.py migrate

echo "Installing Bower components..."
python $PROJ_DIR/manage.py bower install

echo "Collecting static files..."
python $PROJ_DIR/manage.py collectstatic
